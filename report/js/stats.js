var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "15",
        "ok": "15",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "315",
        "ok": "315",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1611",
        "ok": "1611",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1088",
        "ok": "1088",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "296",
        "ok": "296",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1124",
        "ok": "1124",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1271",
        "ok": "1271",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1569",
        "ok": "1569",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 53
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 33
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.5",
        "ok": "0.5",
        "ko": "-"
    }
},
contents: {
"req_open-main-page-5488a": {
        type: "REQUEST",
        name: "open main page",
path: "open main page",
pathFormatted: "req_open-main-page-5488a",
stats: {
    "name": "open main page",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1304",
        "ok": "1304",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1611",
        "ok": "1611",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1313",
        "ok": "1313",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1462",
        "ok": "1462",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1581",
        "ok": "1581",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1605",
        "ok": "1605",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_open-computer-p-96b2a": {
        type: "REQUEST",
        name: "open computer page",
path: "open computer page",
pathFormatted: "req_open-computer-p-96b2a",
stats: {
    "name": "open computer page",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1086",
        "ok": "1086",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1124",
        "ok": "1124",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1107",
        "ok": "1107",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1123",
        "ok": "1123",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1124",
        "ok": "1124",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_open-desktops-p-bdde0": {
        type: "REQUEST",
        name: "open desktops page",
path: "open desktops page",
pathFormatted: "req_open-desktops-p-bdde0",
stats: {
    "name": "open desktops page",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1113",
        "ok": "1113",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1150",
        "ok": "1150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1130",
        "ok": "1130",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "15",
        "ok": "15",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1127",
        "ok": "1127",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1139",
        "ok": "1139",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1148",
        "ok": "1148",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1150",
        "ok": "1150",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_add-product-to--49c99": {
        type: "REQUEST",
        name: "add product to cart",
path: "add product to cart",
pathFormatted: "req_add-product-to--49c99",
stats: {
    "name": "add product to cart",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "315",
        "ok": "315",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "961",
        "ok": "961",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "745",
        "ok": "745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "304",
        "ok": "304",
        "ko": "-"
    },
    "percentiles1": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "percentiles2": {
        "total": "960",
        "ok": "960",
        "ko": "-"
    },
    "percentiles3": {
        "total": "961",
        "ok": "961",
        "ko": "-"
    },
    "percentiles4": {
        "total": "961",
        "ok": "961",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 33
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 67
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_open-cart-page-fb369": {
        type: "REQUEST",
        name: "open cart page",
path: "open cart page",
pathFormatted: "req_open-cart-page-fb369",
stats: {
    "name": "open cart page",
    "numberOfRequests": {
        "total": "3",
        "ok": "3",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "603",
        "ok": "603",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1273",
        "ok": "1273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1048",
        "ok": "1048",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "315",
        "ok": "315",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1268",
        "ok": "1268",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1271",
        "ok": "1271",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1273",
        "ok": "1273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1273",
        "ok": "1273",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 33
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
