package demokaren

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class Demo extends Simulation {

	val httpProtocol = http
		.baseUrl("https://demo.nopcommerce.com")
		.acceptHeader("image/webp,*/*")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0")

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map("Accept" -> "*/*")

	val headers_12 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate")

	val headers_14 = Map("Accept" -> "text/css,*/*;q=0.1")

	val headers_24 = Map(
		"Accept" -> "*/*",
		"Origin" -> "https://demo.nopcommerce.com",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_31 = Map(
		"Accept" -> "*/*",
		"Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
		"Origin" -> "https://demo.nopcommerce.com",
		"X-Requested-With" -> "XMLHttpRequest")

	val scn = scenario("Demo")
		.exec(http("open main page")
			.get("/").check(regex("<title>(.+?)</title>").is("nopCommerce demo store"))
			.headers(headers_0))

		.pause(1)
		.exec(http("open computers page")
			.get("/computers")
			.headers(headers_0)
		.resources(http("request_21")
			.get("/images/thumbs/0000004_software_450.jpg")))

		.pause(147)
		.exec(http("open desktops page")
			.get("/desktops")
			.headers(headers_0))

		.pause(1)
		.exec(http("click on add to cart button")
			.post("/addproducttocart/catalog/2/1/1")
			.headers(headers_24))

		.pause(1)
		.exec(http("open cart page")
			.get("/cart")
			.headers(headers_0))

		http("request_31")
			.post("/shoppingcart/selectshippingoption")
			.headers(headers_31)
			.formParam("itemquantity11215", "1")
			.formParam("CountryId", "0")
			.formParam("StateProvinceId", "0")
			.formParam("ZipPostalCode", "")
			.formParam("checkout_attribute_1", "1")
			.formParam("discountcouponcode", "")
			.formParam("giftcardcouponcode", "")
			.formParam("__RequestVerificationToken", "CfDJ8NJzpPdWJDZGtf_4GVVpZ2lN9ix7auDEVcxXdNAVNZ3UJvXf63p-ewMeuZG-TIZYcnNzcWFa83M5ey003k-VxuUq9fETECM0ralQA5xfjpsw-tZns5x4vhP0qCkB-mfIyGhaVr4TUaTE8OZF0bPwzN4");
            http("request_32")
			.post("/shoppingcart/checkoutattributechange?isEditable=True")
			.headers(headers_31)
			.formParam("itemquantity11215", "1")
			.formParam("CountryId", "0")
			.formParam("StateProvinceId", "0")
			.formParam("ZipPostalCode", "")
			.formParam("checkout_attribute_1", "1")
			.formParam("discountcouponcode", "")
			.formParam("giftcardcouponcode", "")
			.formParam("__RequestVerificationToken", "CfDJ8NJzpPdWJDZGtf_4GVVpZ2lN9ix7auDEVcxXdNAVNZ3UJvXf63p-ewMeuZG-TIZYcnNzcWFa83M5ey003k-VxuUq9fETECM0ralQA5xfjpsw-tZns5x4vhP0qCkB-mfIyGhaVr4TUaTE8OZF0bPwzN4");

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}