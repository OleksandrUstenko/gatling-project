package demokaren

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class demo1 extends Simulation {

	val httpProtocol = http
		.baseUrl("https://demo.nopcommerce.com")
		.acceptHeader("image/webp,*/*")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0")
		.disableFollowRedirect

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_2 = Map("Accept" -> "*/*")

	val headers_7 = Map("Accept" -> "text/css,*/*;q=0.1")

	val headers_23 = Map(
		"Accept" -> "*/*",
		"Origin" -> "https://demo.nopcommerce.com",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_25 = Map(
		"Accept" -> "*/*",
		"Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
		"Origin" -> "https://demo.nopcommerce.com",
		"X-Requested-With" -> "XMLHttpRequest")


	val scn = scenario("demo1")
		.exec(http("open main page")
			.get("/").check(regex("<title>(.+?)</title>").is("nopCommerce demo store"))
			.headers(headers_0))

		.pause(7)
		.exec(http("open computer page")
			.get("/computers").check(status.is(200))
			.headers(headers_0))

		.pause(3)
		.exec(http("open desktops page")
			.get("/desktops").check(regex("<html lang=en class=(.+?)>").is("html-category-page"))
			.headers(headers_0))

		.pause(2)
		.exec(http("add product to cart")
			.post("/addproducttocart/catalog/2/1/1")
			.check(jsonPath("$..message").is("The product has been added to your <a href=\\\"/cart\\\">shopping cart</a>"))
			.headers(headers_23))

		.pause(2)
		.exec(http("open cart page")
			.get("/cart").check(regex("<html lang=en class=(.+?)>").is("html-shopping-cart-page"))
			.headers(headers_0))

	setUp(scn.inject(rampUsers(3)during(15 seconds)))
		.assertions(global.successfulRequests.percent.gt(95)).protocols(httpProtocol)
}