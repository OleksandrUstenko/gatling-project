var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "46",
        "ok": "45",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "950"
    },
    "maxResponseTime": {
        "total": "1853",
        "ok": "1853",
        "ko": "950"
    },
    "meanResponseTime": {
        "total": "273",
        "ok": "258",
        "ko": "950"
    },
    "standardDeviation": {
        "total": "422",
        "ok": "414",
        "ko": "0"
    },
    "percentiles1": {
        "total": "114",
        "ok": "111",
        "ko": "950"
    },
    "percentiles2": {
        "total": "199",
        "ok": "194",
        "ko": "950"
    },
    "percentiles3": {
        "total": "1232",
        "ok": "1239",
        "ko": "950"
    },
    "percentiles4": {
        "total": "1622",
        "ok": "1627",
        "ko": "950"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 39,
    "percentage": 85
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 7
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.187",
        "ok": "0.183",
        "ko": "0.004"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_qhd2yleepc-jlot-a9ed1": {
        type: "REQUEST",
        name: "qhd2yleepc_jlote-mu6hm0u5ov-hp2vfvfd-kzezis.min.css",
path: "qhd2yleepc_jlote-mu6hm0u5ov-hp2vfvfd-kzezis.min.css",
pathFormatted: "req_qhd2yleepc-jlot-a9ed1",
stats: {
    "name": "qhd2yleepc_jlote-mu6hm0u5ov-hp2vfvfd-kzezis.min.css",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "percentiles2": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "percentiles3": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "percentiles4": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_logo-png-1bb87": {
        type: "REQUEST",
        name: "logo.png",
path: "logo.png",
pathFormatted: "req_logo-png-1bb87",
stats: {
    "name": "logo.png",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "122",
        "ok": "122",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles1": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "percentiles2": {
        "total": "102",
        "ok": "102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "118",
        "ok": "118",
        "ko": "-"
    },
    "percentiles4": {
        "total": "121",
        "ok": "121",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.008",
        "ok": "0.008",
        "ko": "-"
    }
}
    },"req_0000079-banner--85b57": {
        type: "REQUEST",
        name: "0000079_banner_1.jpg",
path: "0000079_banner_1.jpg",
pathFormatted: "req_0000079-banner--85b57",
stats: {
    "name": "0000079_banner_1.jpg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "percentiles2": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "percentiles3": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "percentiles4": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000080-banner--81eb8": {
        type: "REQUEST",
        name: "0000080_banner_2.jpg",
path: "0000080_banner_2.jpg",
pathFormatted: "req_0000080-banner--81eb8",
stats: {
    "name": "0000080_banner_2.jpg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles3": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000005-electro-ddfbf": {
        type: "REQUEST",
        name: "0000005_electronics_450.jpeg",
path: "0000005_electronics_450.jpeg",
pathFormatted: "req_0000005-electro-ddfbf",
stats: {
    "name": "0000005_electronics_450.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles2": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles3": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles4": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000009-apparel-a269a": {
        type: "REQUEST",
        name: "0000009_apparel_450.jpeg",
path: "0000009_apparel_450.jpeg",
pathFormatted: "req_0000009-apparel-a269a",
stats: {
    "name": "0000009_apparel_450.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000013-digital-0c1b2": {
        type: "REQUEST",
        name: "0000013_digital-downloads_450.jpeg",
path: "0000013_digital-downloads_450.jpeg",
pathFormatted: "req_0000013-digital-0c1b2",
stats: {
    "name": "0000013_digital-downloads_450.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000020-build-y-ac196": {
        type: "REQUEST",
        name: "0000020_build-your-own-computer_415.jpeg",
path: "0000020_build-your-own-computer_415.jpeg",
pathFormatted: "req_0000020-build-y-ac196",
stats: {
    "name": "0000020_build-your-own-computer_415.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000024-apple-m-87c86": {
        type: "REQUEST",
        name: "0000024_apple-macbook-pro-13-inch_415.jpeg",
path: "0000024_apple-macbook-pro-13-inch_415.jpeg",
pathFormatted: "req_0000024-apple-m-87c86",
stats: {
    "name": "0000024_apple-macbook-pro-13-inch_415.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000041-htc-one-c8675": {
        type: "REQUEST",
        name: "0000041_htc-one-m8-android-l-50-lollipop_415.jpeg",
path: "0000041_htc-one-m8-android-l-50-lollipop_415.jpeg",
pathFormatted: "req_0000041-htc-one-c8675",
stats: {
    "name": "0000041_htc-one-m8-android-l-50-lollipop_415.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles3": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000074-25-virt-64141": {
        type: "REQUEST",
        name: "0000074_25-virtual-gift-card_415.jpeg",
path: "0000074_25-virtual-gift-card_415.jpeg",
pathFormatted: "req_0000074-25-virt-64141",
stats: {
    "name": "0000074_25-virtual-gift-card_415.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_kjzy1mfbtwtm9zg-33d84": {
        type: "REQUEST",
        name: "kjzy1mfbtwtm9zgjw9hl6h1ultbl5nkaj7i9v-g5yaw.min.js",
path: "kjzy1mfbtwtm9zgjw9hl6h1ultbl5nkaj7i9v-g5yaw.min.js",
pathFormatted: "req_kjzy1mfbtwtm9zg-33d84",
stats: {
    "name": "kjzy1mfbtwtm9zgjw9hl6h1ultbl5nkaj7i9v-g5yaw.min.js",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "percentiles2": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "percentiles3": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "percentiles4": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "percentiles2": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "percentiles3": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "percentiles4": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles2": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles3": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles4": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "percentiles2": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "percentiles3": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "percentiles4": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-13-5cca6": {
        type: "REQUEST",
        name: "request_13",
path: "request_13",
pathFormatted: "req_request-13-5cca6",
stats: {
    "name": "request_13",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1265",
        "ok": "1265",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1265",
        "ok": "1265",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1265",
        "ok": "1265",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1265",
        "ok": "1265",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1265",
        "ok": "1265",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1265",
        "ok": "1265",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1265",
        "ok": "1265",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000002-desktop-bd7e1": {
        type: "REQUEST",
        name: "0000002_desktops_450.jpg",
path: "0000002_desktops_450.jpg",
pathFormatted: "req_0000002-desktop-bd7e1",
stats: {
    "name": "0000002_desktops_450.jpg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles3": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_r3-arn7lqlgkkb--2b2d5": {
        type: "REQUEST",
        name: "r3_arn7lqlgkkb-enyo4ix8teuduv8r_tawcmcuc_xk.min.css",
path: "r3_arn7lqlgkkb-enyo4ix8teuduv8r_tawcmcuc_xk.min.css",
pathFormatted: "req_r3-arn7lqlgkkb--2b2d5",
stats: {
    "name": "r3_arn7lqlgkkb-enyo4ix8teuduv8r_tawcmcuc_xk.min.css",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles2": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000004-softwar-c7464": {
        type: "REQUEST",
        name: "0000004_software_450.jpg",
path: "0000004_software_450.jpg",
pathFormatted: "req_0000004-softwar-c7464",
stats: {
    "name": "0000004_software_450.jpg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles2": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles3": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles4": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000003-noteboo-a089c": {
        type: "REQUEST",
        name: "0000003_notebooks_450.jpg",
path: "0000003_notebooks_450.jpg",
pathFormatted: "req_0000003-noteboo-a089c",
stats: {
    "name": "0000003_notebooks_450.jpg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles3": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_9xwqo2cd-vv0ehz-47821": {
        type: "REQUEST",
        name: "9xwqo2cd-vv0ehz4wysrw5d-ck5umvdmfi02qwx3h-s.min.js",
path: "9xwqo2cd-vv0ehz4wysrw5d-ck5umvdmfi02qwx3h-s.min.js",
pathFormatted: "req_9xwqo2cd-vv0ehz-47821",
stats: {
    "name": "9xwqo2cd-vv0ehz4wysrw5d-ck5umvdmfi02qwx3h-s.min.js",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "percentiles2": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "percentiles3": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "percentiles4": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "percentiles2": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "percentiles3": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "percentiles4": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-15-56eac": {
        type: "REQUEST",
        name: "request_15",
path: "request_15",
pathFormatted: "req_request-15-56eac",
stats: {
    "name": "request_15",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "percentiles2": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "percentiles3": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "percentiles4": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-16-24733": {
        type: "REQUEST",
        name: "request_16",
path: "request_16",
pathFormatted: "req_request-16-24733",
stats: {
    "name": "request_16",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1134",
        "ok": "1134",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1134",
        "ok": "1134",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1134",
        "ok": "1134",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1134",
        "ok": "1134",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1134",
        "ok": "1134",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1134",
        "ok": "1134",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1134",
        "ok": "1134",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-20-6804b": {
        type: "REQUEST",
        name: "request_20",
path: "request_20",
pathFormatted: "req_request-20-6804b",
stats: {
    "name": "request_20",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-21-be4cb": {
        type: "REQUEST",
        name: "request_21",
path: "request_21",
pathFormatted: "req_request-21-be4cb",
stats: {
    "name": "request_21",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles2": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles4": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-23-98f5d": {
        type: "REQUEST",
        name: "request_23",
path: "request_23",
pathFormatted: "req_request-23-98f5d",
stats: {
    "name": "request_23",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-25-20ee6": {
        type: "REQUEST",
        name: "request_25",
path: "request_25",
pathFormatted: "req_request-25-20ee6",
stats: {
    "name": "request_25",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles2": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-26-18b3c": {
        type: "REQUEST",
        name: "request_26",
path: "request_26",
pathFormatted: "req_request-26-18b3c",
stats: {
    "name": "request_26",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-24-dd0c9": {
        type: "REQUEST",
        name: "request_24",
path: "request_24",
pathFormatted: "req_request-24-dd0c9",
stats: {
    "name": "request_24",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "976",
        "ok": "976",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "976",
        "ok": "976",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "976",
        "ok": "976",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "976",
        "ok": "976",
        "ko": "-"
    },
    "percentiles2": {
        "total": "976",
        "ok": "976",
        "ko": "-"
    },
    "percentiles3": {
        "total": "976",
        "ok": "976",
        "ko": "-"
    },
    "percentiles4": {
        "total": "976",
        "ok": "976",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1339",
        "ok": "1339",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1339",
        "ok": "1339",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1339",
        "ok": "1339",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1339",
        "ok": "1339",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1339",
        "ok": "1339",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1339",
        "ok": "1339",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1339",
        "ok": "1339",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_s5vxebha6ypxdjt-d3f24": {
        type: "REQUEST",
        name: "s5vxebha6ypxdjtngmp6q5auryjqo20mffzqhnenj0u.min.css",
path: "s5vxebha6ypxdjtngmp6q5auryjqo20mffzqhnenj0u.min.css",
pathFormatted: "req_s5vxebha6ypxdjt-d3f24",
stats: {
    "name": "s5vxebha6ypxdjtngmp6q5auryjqo20mffzqhnenj0u.min.css",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles2": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles3": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles4": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000022-digital-0d6c9": {
        type: "REQUEST",
        name: "0000022_digital-storm-vanquish-3-custom-performance-pc_70.jpeg",
path: "0000022_digital-storm-vanquish-3-custom-performance-pc_70.jpeg",
pathFormatted: "req_0000022-digital-0d6c9",
stats: {
    "name": "0000022_digital-storm-vanquish-3-custom-performance-pc_70.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_0000022-digital-9b309": {
        type: "REQUEST",
        name: "0000022_digital-storm-vanquish-3-custom-performance-pc_80.jpeg",
path: "0000022_digital-storm-vanquish-3-custom-performance-pc_80.jpeg",
pathFormatted: "req_0000022-digital-9b309",
stats: {
    "name": "0000022_digital-storm-vanquish-3-custom-performance-pc_80.jpeg",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles2": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles3": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles4": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_sbovzqjcfikrs23-e0928": {
        type: "REQUEST",
        name: "sbovzqjcfikrs23eca3diwenchabyjgtmmhypne48l0.min.js",
path: "sbovzqjcfikrs23eca3diwenchabyjgtmmhypne48l0.min.js",
pathFormatted: "req_sbovzqjcfikrs23-e0928",
stats: {
    "name": "sbovzqjcfikrs23eca3diwenchabyjgtmmhypne48l0.min.js",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "percentiles2": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "percentiles3": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "percentiles4": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-28-297b8": {
        type: "REQUEST",
        name: "request_28",
path: "request_28",
pathFormatted: "req_request-28-297b8",
stats: {
    "name": "request_28",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-29-6a22e": {
        type: "REQUEST",
        name: "request_29",
path: "request_29",
pathFormatted: "req_request-29-6a22e",
stats: {
    "name": "request_29",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-30-b7b42": {
        type: "REQUEST",
        name: "request_30",
path: "request_30",
pathFormatted: "req_request-30-b7b42",
stats: {
    "name": "request_30",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "percentiles2": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "percentiles3": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "percentiles4": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "950",
        "ok": "-",
        "ko": "950"
    },
    "maxResponseTime": {
        "total": "950",
        "ok": "-",
        "ko": "950"
    },
    "meanResponseTime": {
        "total": "950",
        "ok": "-",
        "ko": "950"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "950",
        "ok": "-",
        "ko": "950"
    },
    "percentiles2": {
        "total": "950",
        "ok": "-",
        "ko": "950"
    },
    "percentiles3": {
        "total": "950",
        "ok": "-",
        "ko": "950"
    },
    "percentiles4": {
        "total": "950",
        "ok": "-",
        "ko": "950"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "-",
        "ko": "0.004"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "percentiles2": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "percentiles3": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "percentiles4": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.004",
        "ok": "0.004",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
