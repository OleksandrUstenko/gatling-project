var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "8",
        "ok": "7",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "285"
    },
    "maxResponseTime": {
        "total": "3782",
        "ok": "3782",
        "ko": "285"
    },
    "meanResponseTime": {
        "total": "1029",
        "ok": "1135",
        "ko": "285"
    },
    "standardDeviation": {
        "total": "1104",
        "ok": "1142",
        "ko": "0"
    },
    "percentiles1": {
        "total": "732",
        "ok": "996",
        "ko": "285"
    },
    "percentiles2": {
        "total": "1088",
        "ok": "1099",
        "ko": "285"
    },
    "percentiles3": {
        "total": "2851",
        "ok": "2984",
        "ko": "285"
    },
    "percentiles4": {
        "total": "3596",
        "ok": "3622",
        "ko": "285"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 38
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 38
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 13
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.348",
        "ok": "0.304",
        "ko": "0.043"
    }
},
contents: {
"req_open-main-page-5488a": {
        type: "REQUEST",
        name: "open main page",
path: "open main page",
pathFormatted: "req_open-main-page-5488a",
stats: {
    "name": "open main page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3782",
        "ok": "3782",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3782",
        "ok": "3782",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3782",
        "ok": "3782",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3782",
        "ok": "3782",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3782",
        "ok": "3782",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3782",
        "ok": "3782",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3782",
        "ok": "3782",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_request-20-6804b": {
        type: "REQUEST",
        name: "request_20",
path: "request_20",
pathFormatted: "req_request-20-6804b",
stats: {
    "name": "request_20",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1122",
        "ok": "1122",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1122",
        "ok": "1122",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1122",
        "ok": "1122",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1122",
        "ok": "1122",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1122",
        "ok": "1122",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1122",
        "ok": "1122",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1122",
        "ok": "1122",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_request-21-be4cb": {
        type: "REQUEST",
        name: "request_21",
path: "request_21",
pathFormatted: "req_request-21-be4cb",
stats: {
    "name": "request_21",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "percentiles2": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "percentiles3": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "percentiles4": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_request-23-98f5d": {
        type: "REQUEST",
        name: "request_23",
path: "request_23",
pathFormatted: "req_request-23-98f5d",
stats: {
    "name": "request_23",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "996",
        "ok": "996",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "996",
        "ok": "996",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "996",
        "ok": "996",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "996",
        "ok": "996",
        "ko": "-"
    },
    "percentiles2": {
        "total": "996",
        "ok": "996",
        "ko": "-"
    },
    "percentiles3": {
        "total": "996",
        "ok": "996",
        "ko": "-"
    },
    "percentiles4": {
        "total": "996",
        "ok": "996",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_request-24-dd0c9": {
        type: "REQUEST",
        name: "request_24",
path: "request_24",
pathFormatted: "req_request-24-dd0c9",
stats: {
    "name": "request_24",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles2": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles3": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles4": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_request-25-20ee6": {
        type: "REQUEST",
        name: "request_25",
path: "request_25",
pathFormatted: "req_request-25-20ee6",
stats: {
    "name": "request_25",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "285",
        "ok": "-",
        "ko": "285"
    },
    "maxResponseTime": {
        "total": "285",
        "ok": "-",
        "ko": "285"
    },
    "meanResponseTime": {
        "total": "285",
        "ok": "-",
        "ko": "285"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "285",
        "ok": "-",
        "ko": "285"
    },
    "percentiles2": {
        "total": "285",
        "ok": "-",
        "ko": "285"
    },
    "percentiles3": {
        "total": "285",
        "ok": "-",
        "ko": "285"
    },
    "percentiles4": {
        "total": "285",
        "ok": "-",
        "ko": "285"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "-",
        "ko": "0.043"
    }
}
    },"req_request-26-18b3c": {
        type: "REQUEST",
        name: "request_26",
path: "request_26",
pathFormatted: "req_request-26-18b3c",
stats: {
    "name": "request_26",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
