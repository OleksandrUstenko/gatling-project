var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "8",
        "ok": "7",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "265"
    },
    "maxResponseTime": {
        "total": "1338",
        "ok": "1338",
        "ko": "265"
    },
    "meanResponseTime": {
        "total": "654",
        "ok": "709",
        "ko": "265"
    },
    "standardDeviation": {
        "total": "407",
        "ok": "406",
        "ko": "0"
    },
    "percentiles1": {
        "total": "545",
        "ok": "615",
        "ko": "265"
    },
    "percentiles2": {
        "total": "987",
        "ok": "1015",
        "ko": "265"
    },
    "percentiles3": {
        "total": "1244",
        "ok": "1258",
        "ko": "265"
    },
    "percentiles4": {
        "total": "1319",
        "ok": "1322",
        "ko": "265"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 50
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 25
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 13
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.4",
        "ok": "0.35",
        "ko": "0.05"
    }
},
contents: {
"req_open-main-page-5488a": {
        type: "REQUEST",
        name: "open main page",
path: "open main page",
pathFormatted: "req_open-main-page-5488a",
stats: {
    "name": "open main page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "percentiles2": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "percentiles3": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "percentiles4": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_open-computer-p-96b2a": {
        type: "REQUEST",
        name: "open computer page",
path: "open computer page",
pathFormatted: "req_open-computer-p-96b2a",
stats: {
    "name": "open computer page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1338",
        "ok": "1338",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1338",
        "ok": "1338",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1338",
        "ok": "1338",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1338",
        "ok": "1338",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1338",
        "ok": "1338",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1338",
        "ok": "1338",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1338",
        "ok": "1338",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_request-21-be4cb": {
        type: "REQUEST",
        name: "request_21",
path: "request_21",
pathFormatted: "req_request-21-be4cb",
stats: {
    "name": "request_21",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "percentiles2": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "percentiles3": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "percentiles4": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_request-23-98f5d": {
        type: "REQUEST",
        name: "request_23",
path: "request_23",
pathFormatted: "req_request-23-98f5d",
stats: {
    "name": "request_23",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "959",
        "ok": "959",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "959",
        "ok": "959",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "959",
        "ok": "959",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "959",
        "ok": "959",
        "ko": "-"
    },
    "percentiles2": {
        "total": "959",
        "ok": "959",
        "ko": "-"
    },
    "percentiles3": {
        "total": "959",
        "ok": "959",
        "ko": "-"
    },
    "percentiles4": {
        "total": "959",
        "ok": "959",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_request-24-dd0c9": {
        type: "REQUEST",
        name: "request_24",
path: "request_24",
pathFormatted: "req_request-24-dd0c9",
stats: {
    "name": "request_24",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_request-25-20ee6": {
        type: "REQUEST",
        name: "request_25",
path: "request_25",
pathFormatted: "req_request-25-20ee6",
stats: {
    "name": "request_25",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "265",
        "ok": "-",
        "ko": "265"
    },
    "maxResponseTime": {
        "total": "265",
        "ok": "-",
        "ko": "265"
    },
    "meanResponseTime": {
        "total": "265",
        "ok": "-",
        "ko": "265"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "265",
        "ok": "-",
        "ko": "265"
    },
    "percentiles2": {
        "total": "265",
        "ok": "-",
        "ko": "265"
    },
    "percentiles3": {
        "total": "265",
        "ok": "-",
        "ko": "265"
    },
    "percentiles4": {
        "total": "265",
        "ok": "-",
        "ko": "265"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "-",
        "ko": "0.05"
    }
}
    },"req_request-26-18b3c": {
        type: "REQUEST",
        name: "request_26",
path: "request_26",
pathFormatted: "req_request-26-18b3c",
stats: {
    "name": "request_26",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1070",
        "ok": "1070",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1070",
        "ok": "1070",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1070",
        "ok": "1070",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1070",
        "ok": "1070",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1070",
        "ok": "1070",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1070",
        "ok": "1070",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1070",
        "ok": "1070",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
