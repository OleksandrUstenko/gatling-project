var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "7",
        "ok": "6",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "947"
    },
    "maxResponseTime": {
        "total": "1303",
        "ok": "1303",
        "ko": "947"
    },
    "meanResponseTime": {
        "total": "958",
        "ok": "960",
        "ko": "947"
    },
    "standardDeviation": {
        "total": "287",
        "ok": "310",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1066",
        "ok": "1088",
        "ko": "947"
    },
    "percentiles2": {
        "total": "1156",
        "ok": "1180",
        "ko": "947"
    },
    "percentiles3": {
        "total": "1273",
        "ok": "1278",
        "ko": "947"
    },
    "percentiles4": {
        "total": "1297",
        "ok": "1298",
        "ko": "947"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 29
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 29
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 29
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 14
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.35",
        "ok": "0.3",
        "ko": "0.05"
    }
},
contents: {
"req_open-main-page-5488a": {
        type: "REQUEST",
        name: "open main page",
path: "open main page",
pathFormatted: "req_open-main-page-5488a",
stats: {
    "name": "open main page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_open-computer-p-96b2a": {
        type: "REQUEST",
        name: "open computer page",
path: "open computer page",
pathFormatted: "req_open-computer-p-96b2a",
stats: {
    "name": "open computer page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_open-desktops-p-bdde0": {
        type: "REQUEST",
        name: "open desktops page",
path: "open desktops page",
pathFormatted: "req_open-desktops-p-bdde0",
stats: {
    "name": "open desktops page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "percentiles2": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "percentiles3": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "percentiles4": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_add-product-to--49c99": {
        type: "REQUEST",
        name: "add product to cart",
path: "add product to cart",
pathFormatted: "req_add-product-to--49c99",
stats: {
    "name": "add product to cart",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_open-cart-page-fb369": {
        type: "REQUEST",
        name: "open cart page",
path: "open cart page",
pathFormatted: "req_open-cart-page-fb369",
stats: {
    "name": "open cart page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "626",
        "ok": "626",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "626",
        "ok": "626",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "626",
        "ok": "626",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "626",
        "ok": "626",
        "ko": "-"
    },
    "percentiles2": {
        "total": "626",
        "ok": "626",
        "ko": "-"
    },
    "percentiles3": {
        "total": "626",
        "ok": "626",
        "ko": "-"
    },
    "percentiles4": {
        "total": "626",
        "ok": "626",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    },"req_request-25-20ee6": {
        type: "REQUEST",
        name: "request_25",
path: "request_25",
pathFormatted: "req_request-25-20ee6",
stats: {
    "name": "request_25",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "947",
        "ok": "-",
        "ko": "947"
    },
    "maxResponseTime": {
        "total": "947",
        "ok": "-",
        "ko": "947"
    },
    "meanResponseTime": {
        "total": "947",
        "ok": "-",
        "ko": "947"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "947",
        "ok": "-",
        "ko": "947"
    },
    "percentiles2": {
        "total": "947",
        "ok": "-",
        "ko": "947"
    },
    "percentiles3": {
        "total": "947",
        "ok": "-",
        "ko": "947"
    },
    "percentiles4": {
        "total": "947",
        "ok": "-",
        "ko": "947"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "-",
        "ko": "0.05"
    }
}
    },"req_request-26-18b3c": {
        type: "REQUEST",
        name: "request_26",
path: "request_26",
pathFormatted: "req_request-26-18b3c",
stats: {
    "name": "request_26",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.05",
        "ok": "0.05",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
