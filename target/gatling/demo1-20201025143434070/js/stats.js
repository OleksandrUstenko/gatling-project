var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "939",
        "ok": "939",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "356",
        "ok": "356",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1390",
        "ok": "1390",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 40
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 40
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 20
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.263",
        "ok": "0.263",
        "ko": "-"
    }
},
contents: {
"req_open-main-page-5488a": {
        type: "REQUEST",
        name: "open main page",
path: "open main page",
pathFormatted: "req_open-main-page-5488a",
stats: {
    "name": "open main page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1452",
        "ok": "1452",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.053",
        "ok": "0.053",
        "ko": "-"
    }
}
    },"req_open-computer-p-96b2a": {
        type: "REQUEST",
        name: "open computer page",
path: "open computer page",
pathFormatted: "req_open-computer-p-96b2a",
stats: {
    "name": "open computer page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.053",
        "ok": "0.053",
        "ko": "-"
    }
}
    },"req_open-desktops-p-bdde0": {
        type: "REQUEST",
        name: "open desktops page",
path: "open desktops page",
pathFormatted: "req_open-desktops-p-bdde0",
stats: {
    "name": "open desktops page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "percentiles2": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "percentiles3": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "percentiles4": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.053",
        "ok": "0.053",
        "ko": "-"
    }
}
    },"req_add-product-to--49c99": {
        type: "REQUEST",
        name: "add product to cart",
path: "add product to cart",
pathFormatted: "req_add-product-to--49c99",
stats: {
    "name": "add product to cart",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.053",
        "ok": "0.053",
        "ko": "-"
    }
}
    },"req_open-cart-page-fb369": {
        type: "REQUEST",
        name: "open cart page",
path: "open cart page",
pathFormatted: "req_open-cart-page-fb369",
stats: {
    "name": "open cart page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "640",
        "ok": "640",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "640",
        "ok": "640",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "640",
        "ok": "640",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "640",
        "ok": "640",
        "ko": "-"
    },
    "percentiles2": {
        "total": "640",
        "ok": "640",
        "ko": "-"
    },
    "percentiles3": {
        "total": "640",
        "ok": "640",
        "ko": "-"
    },
    "percentiles4": {
        "total": "640",
        "ok": "640",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.053",
        "ok": "0.053",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
