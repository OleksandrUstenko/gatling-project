var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "5",
        "ok": "4",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "624",
        "ok": "624",
        "ko": "1338"
    },
    "maxResponseTime": {
        "total": "2102",
        "ok": "2102",
        "ko": "1338"
    },
    "meanResponseTime": {
        "total": "1247",
        "ok": "1224",
        "ko": "1338"
    },
    "standardDeviation": {
        "total": "490",
        "ok": "546",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1192",
        "ok": "1085",
        "ko": "1338"
    },
    "percentiles2": {
        "total": "1338",
        "ok": "1420",
        "ko": "1338"
    },
    "percentiles3": {
        "total": "1949",
        "ok": "1965",
        "ko": "1338"
    },
    "percentiles4": {
        "total": "2071",
        "ok": "2075",
        "ko": "1338"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 20
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 40
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 20
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 20
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.238",
        "ok": "0.19",
        "ko": "0.048"
    }
},
contents: {
"req_open-main-page-5488a": {
        type: "REQUEST",
        name: "open main page",
path: "open main page",
pathFormatted: "req_open-main-page-5488a",
stats: {
    "name": "open main page",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1338",
        "ok": "-",
        "ko": "1338"
    },
    "maxResponseTime": {
        "total": "1338",
        "ok": "-",
        "ko": "1338"
    },
    "meanResponseTime": {
        "total": "1338",
        "ok": "-",
        "ko": "1338"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1338",
        "ok": "-",
        "ko": "1338"
    },
    "percentiles2": {
        "total": "1338",
        "ok": "-",
        "ko": "1338"
    },
    "percentiles3": {
        "total": "1338",
        "ok": "-",
        "ko": "1338"
    },
    "percentiles4": {
        "total": "1338",
        "ok": "-",
        "ko": "1338"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.048",
        "ok": "-",
        "ko": "0.048"
    }
}
    },"req_open-computer-p-96b2a": {
        type: "REQUEST",
        name: "open computer page",
path: "open computer page",
pathFormatted: "req_open-computer-p-96b2a",
stats: {
    "name": "open computer page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.048",
        "ok": "0.048",
        "ko": "-"
    }
}
    },"req_open-desktops-p-bdde0": {
        type: "REQUEST",
        name: "open desktops page",
path: "open desktops page",
pathFormatted: "req_open-desktops-p-bdde0",
stats: {
    "name": "open desktops page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1192",
        "ok": "1192",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1192",
        "ok": "1192",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1192",
        "ok": "1192",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1192",
        "ok": "1192",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1192",
        "ok": "1192",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1192",
        "ok": "1192",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1192",
        "ok": "1192",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.048",
        "ok": "0.048",
        "ko": "-"
    }
}
    },"req_add-product-to--49c99": {
        type: "REQUEST",
        name: "add product to cart",
path: "add product to cart",
pathFormatted: "req_add-product-to--49c99",
stats: {
    "name": "add product to cart",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "978",
        "ok": "978",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "978",
        "ok": "978",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "978",
        "ok": "978",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "978",
        "ok": "978",
        "ko": "-"
    },
    "percentiles2": {
        "total": "978",
        "ok": "978",
        "ko": "-"
    },
    "percentiles3": {
        "total": "978",
        "ok": "978",
        "ko": "-"
    },
    "percentiles4": {
        "total": "978",
        "ok": "978",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.048",
        "ok": "0.048",
        "ko": "-"
    }
}
    },"req_open-cart-page-fb369": {
        type: "REQUEST",
        name: "open cart page",
path: "open cart page",
pathFormatted: "req_open-cart-page-fb369",
stats: {
    "name": "open cart page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "percentiles2": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "percentiles3": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "percentiles4": {
        "total": "624",
        "ok": "624",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.048",
        "ok": "0.048",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
