var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "5",
        "ok": "4",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "442",
        "ok": "442",
        "ko": "484"
    },
    "maxResponseTime": {
        "total": "4573",
        "ok": "4573",
        "ko": "484"
    },
    "meanResponseTime": {
        "total": "1605",
        "ok": "1886",
        "ko": "484"
    },
    "standardDeviation": {
        "total": "1527",
        "ok": "1587",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1237",
        "ok": "1264",
        "ko": "484"
    },
    "percentiles2": {
        "total": "1291",
        "ok": "2112",
        "ko": "484"
    },
    "percentiles3": {
        "total": "3917",
        "ok": "4081",
        "ko": "484"
    },
    "percentiles4": {
        "total": "4442",
        "ok": "4475",
        "ko": "484"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 20
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 60
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 20
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.217",
        "ok": "0.174",
        "ko": "0.043"
    }
},
contents: {
"req_open-main-page-5488a": {
        type: "REQUEST",
        name: "open main page",
path: "open main page",
pathFormatted: "req_open-main-page-5488a",
stats: {
    "name": "open main page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4573",
        "ok": "4573",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4573",
        "ok": "4573",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4573",
        "ok": "4573",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4573",
        "ok": "4573",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4573",
        "ok": "4573",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4573",
        "ok": "4573",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4573",
        "ok": "4573",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_open-computer-p-96b2a": {
        type: "REQUEST",
        name: "open computer page",
path: "open computer page",
pathFormatted: "req_open-computer-p-96b2a",
stats: {
    "name": "open computer page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1237",
        "ok": "1237",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1237",
        "ok": "1237",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1237",
        "ok": "1237",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1237",
        "ok": "1237",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1237",
        "ok": "1237",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1237",
        "ok": "1237",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1237",
        "ok": "1237",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_open-desktops-p-bdde0": {
        type: "REQUEST",
        name: "open desktops page",
path: "open desktops page",
pathFormatted: "req_open-desktops-p-bdde0",
stats: {
    "name": "open desktops page",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "484",
        "ok": "-",
        "ko": "484"
    },
    "maxResponseTime": {
        "total": "484",
        "ok": "-",
        "ko": "484"
    },
    "meanResponseTime": {
        "total": "484",
        "ok": "-",
        "ko": "484"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "484",
        "ok": "-",
        "ko": "484"
    },
    "percentiles2": {
        "total": "484",
        "ok": "-",
        "ko": "484"
    },
    "percentiles3": {
        "total": "484",
        "ok": "-",
        "ko": "484"
    },
    "percentiles4": {
        "total": "484",
        "ok": "-",
        "ko": "484"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "-",
        "ko": "0.043"
    }
}
    },"req_add-product-to--49c99": {
        type: "REQUEST",
        name: "add product to cart",
path: "add product to cart",
pathFormatted: "req_add-product-to--49c99",
stats: {
    "name": "add product to cart",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    },"req_open-cart-page-fb369": {
        type: "REQUEST",
        name: "open cart page",
path: "open cart page",
pathFormatted: "req_open-cart-page-fb369",
stats: {
    "name": "open cart page",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "percentiles2": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "percentiles3": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "percentiles4": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.043",
        "ok": "0.043",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
